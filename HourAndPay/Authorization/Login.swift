//
//  Login.swift
//  HourAndPay
//
//  Created by Prajwal Maharjan on 11/20/17.
//  Copyright © 2017 Prajwal Maharjan. All rights reserved.
//

import ObjectMapper
import RealmSwift

class Login: Object, Mappable {
    @objc dynamic var id: String?
    @objc dynamic var tokenType: String?
    @objc dynamic var expiresIn: String?
    @objc dynamic var accessToken: String?
    @objc dynamic var refreshToken: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
//    required init?(map: Map) {
//
//    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        tokenType <- map["token_type"]
        expiresIn <- map["expires_in"]
        accessToken <- map["access_token"]
        refreshToken <- map["refresh_token"]
    }
}
