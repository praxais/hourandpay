//
//  CompanyInteractor.swift
//  HourAndPay
//
//  Created by Prajwal Maharjan on 11/20/17.
//  Copyright © 2017 Prajwal Maharjan. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import RealmSwift

class CompanyInteractor {
    public func doLogin(email: String, password: String, completion: @escaping () -> ()){
        let parameters: Parameters = [
            "client_id" : ApiConstants.clientId,
            "client_secret": ApiConstants.clientSecret,
            "grant_type": ApiConstants.grantTypePassword,
            "username" : email,
            "password": password,
            "client_type": ApiConstants.clientTypeAdmin,
            "scope": ApiConstants.scope
        ]
        
        Alamofire.request(ApiConstants.baseUrl + ApiConstants.loginUrl,method: .post, parameters: parameters)
            .responseObject { (response: DataResponse<Login>) in
                switch response.result {
                    case .success:
                        let login = response.result.value
                        login?.id = "1"
                        
                        let realm = try! Realm()
                        try! realm.write {
                            realm.add(login!, update: true)
                        }
                        completion()
                        break
                    case .failure:
                        print("Xais: \(response.error.debugDescription)")
                        break
                }
        }
    }
}
