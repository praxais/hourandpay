//
//  CompanyViewController.swift
//  HourAndPay
//
//  Created by Prajwal Maharjan on 11/20/17.
//  Copyright © 2017 Prajwal Maharjan. All rights reserved.
//

import UIKit

class CompanyViewController: UIViewController {
    private let companyInteractor = CompanyInteractor()
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onBackClicked(_ sender: UIButton) {
//        self.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true)
    }
    
    @IBAction func onSubmitClicked(_ sender: UIButton) {
        let indicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        indicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        indicator.center = view.center
        view.addSubview(indicator)
        indicator.bringSubview(toFront: view)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        indicator.startAnimating()
        companyInteractor.doLogin(email: email.text!, password: password.text!) {
            UIApplication.shared.endIgnoringInteractionEvents()
            indicator.stopAnimating()
            
            //open Landing Screen
            let storyboard = UIStoryboard(name: "Landing", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TabBarScreen") // MySecondSecreen the storyboard ID
            self.present(vc, animated: true, completion: nil)
        }
    }
}
