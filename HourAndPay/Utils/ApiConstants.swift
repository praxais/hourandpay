//
//  ApiConstants.swift
//  HourAndPay
//
//  Created by Prajwal Maharjan on 11/20/17.
//  Copyright © 2017 Prajwal Maharjan. All rights reserved.
//

public class ApiConstants {
    public static let baseUrl = "http://api.dev-hnp.ekbana.info/"
    
    public static let loginUrl = "api/v1/auth/login"
    
    public static let clientId = 2
    public static let clientSecret = "1KSwWgafVDiTTMZWafCAsRGNwTpRI6ekjWbsF0jw"
    public static let grantTypePassword = "password"
    public static let grantTypeRefreshToken = "refresh_token"
    public static let clientTypeAdmin = "employee_admin"
    public static let clientTypeUser = "employee"
    public static let scope = "*"
}
